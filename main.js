// 1. DOM - це веб-сторінка, що являє собою дерево елементів, які представляються як об'єкти, якими можна маніпулювати за допомогою JS.
// 2. innerHTML виводить строку з HTML включаючи теги, а innerText - тільки текст.
// 3. Найзручніший спосіб є використання querySelector, але є інші методи(id, тегом, классом).

const paragraphs = document.querySelector("p");
paragraphs.style.backgroundColor = "red";

const elementById = document.getElementById("optionsList");
console.log(elementById);
console.log(elementById.closest("ul"));
console.log(elementById.childNodes);
console.log(elementById.nodeType);
console.log(elementById.nodeName);

const testParagraph = document.getElementById("testParagraph").innerText = "This is a paragraph";
console.log(testParagraph);

const childElements = document.querySelector(".main-header").childNodes;
childElements.forEach((el) => {el.className = "nav-item"});
console.log(childElements);

const sectionTitleEls = document.querySelectorAll(".section-title");
console.log(sectionTitleEls)
sectionTitleEls.forEach((el) => {el.classList.remove("section-title")})

